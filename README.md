## Objectives

1. Students should review Ruby classes and methods
2. Students will be introduced to the Model/View/Controller web design pattern 
3. Students will practice using git and GitHub to commit and push materials to remote repositories

## Resources

### MVC

* [MVC on Wikipedia](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)
* [MVC Architecture from Chrome](https://developer.chrome.com/apps/app_frameworks)
* [MVC on Learn](https://github.com/learn-co-curriculum/sinatra-intro-to-mvc)
* [Sinatra MVC Review](https://www.youtube.com/watch?v=CX0i52dnTwk)

### Git

* [What is Version Control?](https://github.com/learn-co-curriculum/git-version-control-101)
* [Learn Git in 15 minutes](https://try.github.io/levels/1/challenges/1)
* [Git Repository Basics](https://github.com/learn-co-curriculum/git-basics-readme)
* [ReferenceGuide to git Commands](http://rogerdudler.github.io/git-guide/)

### GitHub

* [What is GitHub](https://guides.github.com/activities/hello-world/)
* [GitHub Flow](https://guides.github.com/introduction/flow/)
* [Forking and Cloning with GitHub](https://github.com/learn-co-curriculum/forks-and-clones-readme)
* [Working with a Remote Git Repository](https://github.com/learn-co-curriculum/git-remotes-with-github-readme)
* [GitHub Website Reference](https://www.pluralsight.com/blog/software-development/github-tutorial)
